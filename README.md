# Tarea 1 INF322
## Nombre: Alexander Alfaro
## Rol: 201830020-9

Tenemos dos problemas importantes en la interfaz donde se crea el album de fotos, el primero:

- La inexistencia de botones para activar o desactivar características, principalmente en las opciones para la sombra y borde, donde para poner una sombra hay que seleccionar un color de sombra, entonces la sombra aparecerá automáticamente y para desactivarla tenemos que bajar la transparencia, lo mismo ocurre en bordes donde para hacer aparecer o desaparecer un borde tenemos que cambiar su tamaño.

![Imagen 1](image_1.png)

- Aquí el usuario desea borrar el borde/sombra que ha creado, y no encuentra un boton que sirva explícitamente para borrarlo, teniendo que cambiar un parámetro para ello.
- Aunque se afecta la eficiencia, el atributo principalmente afectado es la Minimización de Errores, ya que se tienen que cambiar parámetros de el borde/sombra para hacerlos desaparecer, estos parámetros después se pueden olvidar resultando en que se pierda un diseño deseado que se había logrado con anterioridad.

El segundo problema tiene que ver con la compatibilidad con diferentes dispositivos y configuraciones:

- Al usar pantalla dividida en mi PC, se puede ver que el sitio no tiene buen manejo para pantallas pequeñas o con mucho zoom. Los iconos se superponen a la selección de página lo que causa dificultades a la hora de seleccionar opciones.

![Alt text](image_2.png)

- El usuario intenta seleccionar una cara/página distinta y tiene que "hacerle el quite", a los íconos que se superponen, o bajar el zoom.

- Aquí lo que falla es la Minimización de Errores, ya que es muy facil hacer click por error en una opción indeseada.